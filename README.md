# YY3568-Openharmony

## 环境搭建
##### 安装依赖工具

``` C
sudo apt-get update && sudo apt-get install binutils git git-lfs gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc gnutls-bin python3.8 python3-pip ruby
```

##### 获取源代码
1. 注册码云gitee账号。
2. 注册码云SSH公钥，请参考码云帮助中心。
3. 安装git客户端和git-lfs并配置用户信息。
``` C
git config --global user.name "yourname"
git config --global user.email "your-email-address"
git config --global credential.helper store
```

4. 安装码云repo工具，可以执行如下命令。
``` C
curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 \>
/usr/local/bin/repo \#如果没有权限，可下载至其他目录，并将其配置到环境变量中

chmod a+x /usr/local/bin/repo

pip3 install -i https://repo.huaweicloud.com/repository/pypi/simple requests
```

5. 获取源码操作步骤
``` C
repo init -u https://gitee.com/youyeetoo/yy3568-openharmony -b master -m yy3568_ohso4.1.xml
repo sync -c
repo forall -c 'git lfs pull'
```

## 构建YY3568
##### 安装编译器及二进制工具
``` c
bash build/prebuilts_download.sh
```

##### 构建工程

``` C
./build.sh --product-name yy3568 –ccache
```

#### 烧录验证
- 编译完成之后，固件生成到目录：
``` C
youyeetoo@youyeetoo:~/ohos/project/youyeetoo/yy3568/out/yy3568/packages/phone/images$ ls
boot_linux.img  config.cfg         parameter.txt  resource.img  system.img  updater.img   vendor.img
chip_prod.img   MiniLoaderAll.bin  ramdisk.img    sys_prod.img  uboot.img   userdata.img
youyeetoo@youyeetoo:~/ohos/project/youyeetoo/yy3568/out/yy3568/packages/phone/images$ 
```

- 使用RKDevTool.exe烧录固件，在镜像目录中存在一个config.cfg，在RKDevTool.exe导入该配置，导入完配置确定一下你的镜像目录是不是全部正确。

![](./assert/dl1.png)

- 板子按住RECOVERY键，然后上电，这时设备进入loader模式，RKDevTool.exe会显示发现一个LOADER设备，点击执行。

![](./assert/dl2.png)

